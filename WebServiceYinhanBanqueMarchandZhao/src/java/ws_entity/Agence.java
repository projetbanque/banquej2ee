/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws_entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author l.marchand
 */
@Entity
@Table(name = "AGENCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Agence.findAll", query = "SELECT a FROM Agence a")
    , @NamedQuery(name = "Agence.findById", query = "SELECT a FROM Agence a WHERE a.id = :id")
    , @NamedQuery(name = "Agence.findBySwiftcodeagence", query = "SELECT a FROM Agence a WHERE a.swiftcodeagence = :swiftcodeagence")
    , @NamedQuery(name = "Agence.findByCodepostalagence", query = "SELECT a FROM Agence a WHERE a.codepostalagence = :codepostalagence")
    , @NamedQuery(name = "Agence.findByEmailagence", query = "SELECT a FROM Agence a WHERE a.emailagence = :emailagence")
    , @NamedQuery(name = "Agence.findByNumerorueagence", query = "SELECT a FROM Agence a WHERE a.numerorueagence = :numerorueagence")
    , @NamedQuery(name = "Agence.findByPaysagence", query = "SELECT a FROM Agence a WHERE a.paysagence = :paysagence")
    , @NamedQuery(name = "Agence.findByRueagence", query = "SELECT a FROM Agence a WHERE a.rueagence = :rueagence")
    , @NamedQuery(name = "Agence.findByTelephoneagence", query = "SELECT a FROM Agence a WHERE a.telephoneagence = :telephoneagence")
    , @NamedQuery(name = "Agence.findByVilleagence", query = "SELECT a FROM Agence a WHERE a.villeagence = :villeagence")})
public class Agence implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)

    @Column(name = "ID")
    private Long id;
    @Column(name = "SWIFTCODEAGENCE")
    private String swiftcodeagence;
    @Column(name = "CODEPOSTALAGENCE")
    private String codepostalagence;
    @Column(name = "EMAILAGENCE")
    private String emailagence;
    @Column(name = "NUMERORUEAGENCE")
    private Integer numerorueagence;
    @Column(name = "PAYSAGENCE")
    private String paysagence;
    @Column(name = "RUEAGENCE")
    private String rueagence;
    @Column(name = "TELEPHONEAGENCE")
    private String telephoneagence;
    @Column(name = "VILLEAGENCE")
    private String villeagence;
    @OneToMany(mappedBy = "idAgence")
    private Collection<Banquier> banquierCollection;

    public Agence() {
    }

    public Agence(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSwiftcodeagence() {
        return swiftcodeagence;
    }

    public void setSwiftcodeagence(String swiftcodeagence) {
        this.swiftcodeagence = swiftcodeagence;
    }

    public String getCodepostalagence() {
        return codepostalagence;
    }

    public void setCodepostalagence(String codepostalagence) {
        this.codepostalagence = codepostalagence;
    }

    public String getEmailagence() {
        return emailagence;
    }

    public void setEmailagence(String emailagence) {
        this.emailagence = emailagence;
    }

    public Integer getNumerorueagence() {
        return numerorueagence;
    }

    public void setNumerorueagence(Integer numerorueagence) {
        this.numerorueagence = numerorueagence;
    }

    public String getPaysagence() {
        return paysagence;
    }

    public void setPaysagence(String paysagence) {
        this.paysagence = paysagence;
    }

    public String getRueagence() {
        return rueagence;
    }

    public void setRueagence(String rueagence) {
        this.rueagence = rueagence;
    }

    public String getTelephoneagence() {
        return telephoneagence;
    }

    public void setTelephoneagence(String telephoneagence) {
        this.telephoneagence = telephoneagence;
    }

    public String getVilleagence() {
        return villeagence;
    }

    public void setVilleagence(String villeagence) {
        this.villeagence = villeagence;
    }

    @XmlTransient
    public Collection<Banquier> getBanquierCollection() {
        return banquierCollection;
    }

    public void setBanquierCollection(Collection<Banquier> banquierCollection) {
        this.banquierCollection = banquierCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Agence)) {
            return false;
        }
        Agence other = (Agence) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ws_entity.Agence[ id=" + id + " ]";
    }
    
}
