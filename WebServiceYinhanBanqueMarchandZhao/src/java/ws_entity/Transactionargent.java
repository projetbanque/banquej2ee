/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws_entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author l.marchand
 */
@Entity
@Table(name = "TRANSACTIONARGENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Transactionargent.findAll", query = "SELECT t FROM Transactionargent t")
    , @NamedQuery(name = "Transactionargent.findById", query = "SELECT t FROM Transactionargent t WHERE t.id = :id")
    , @NamedQuery(name = "Transactionargent.findByDatetransfert", query = "SELECT t FROM Transactionargent t WHERE t.datetransfert = :datetransfert")
    , @NamedQuery(name = "Transactionargent.findByLibelle", query = "SELECT t FROM Transactionargent t WHERE t.libelle = :libelle")
    , @NamedQuery(name = "Transactionargent.findBySommetransferee", query = "SELECT t FROM Transactionargent t WHERE t.sommetransferee = :sommetransferee")
    , @NamedQuery(name = "Transactionargent.findByStatus", query = "SELECT t FROM Transactionargent t WHERE t.status = :status")})
public class Transactionargent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "DATETRANSFERT")
    @Temporal(TemporalType.DATE)
    private Date datetransfert;
    @Column(name = "LIBELLE")
    private String libelle;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SOMMETRANSFEREE")
    private Double sommetransferee;
    @Column(name = "STATUS")
    private Integer status;
    @JoinColumn(name = "ID_COMPTECREDIT", referencedColumnName = "ID")
    @ManyToOne
    private Comptebancaire idComptecredit;
    @JoinColumn(name = "ID_COMPTEDEBIT", referencedColumnName = "ID")
    @ManyToOne
    private Comptebancaire idComptedebit;

    public Transactionargent() {
    }

    public Transactionargent(Long id) {
        this.id = id;
    }

    public Transactionargent(Date datetransfert, String libelle, Double sommetransferee, Integer status, Comptebancaire idComptecredit, Comptebancaire idComptedebit) {
        this.datetransfert = datetransfert;
        this.libelle = libelle;
        this.sommetransferee = sommetransferee;
        this.status = status;
        this.idComptecredit = idComptecredit;
        this.idComptedebit = idComptedebit;
    }
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDatetransfert() {
        return datetransfert;
    }

    public void setDatetransfert(Date datetransfert) {
        this.datetransfert = datetransfert;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Double getSommetransferee() {
        return sommetransferee;
    }

    public void setSommetransferee(Double sommetransferee) {
        this.sommetransferee = sommetransferee;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Comptebancaire getIdComptecredit() {
        return idComptecredit;
    }

    public void setIdComptecredit(Comptebancaire idComptecredit) {
        this.idComptecredit = idComptecredit;
    }

    public Comptebancaire getIdComptedebit() {
        return idComptedebit;
    }

    public void setIdComptedebit(Comptebancaire idComptedebit) {
        this.idComptedebit = idComptedebit;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transactionargent)) {
            return false;
        }
        Transactionargent other = (Transactionargent) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ws_entity.Transactionargent[ id=" + id + " ]";
    }
    
}
