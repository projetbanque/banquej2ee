/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws_entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author l.marchand
 */
@Entity
@Table(name = "BANQUIER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Banquier.findAll", query = "SELECT b FROM Banquier b")
    , @NamedQuery(name = "Banquier.findById", query = "SELECT b FROM Banquier b WHERE b.id = :id")
    , @NamedQuery(name = "Banquier.findByEmailbanquier", query = "SELECT b FROM Banquier b WHERE b.emailbanquier = :emailbanquier")
    , @NamedQuery(name = "Banquier.findByIdentifiantbanquier", query = "SELECT b FROM Banquier b WHERE b.identifiantbanquier = :identifiantbanquier")
    , @NamedQuery(name = "Banquier.findByMotdepassebanquier", query = "SELECT b FROM Banquier b WHERE b.motdepassebanquier = :motdepassebanquier")
    , @NamedQuery(name = "Banquier.findByNombanquier", query = "SELECT b FROM Banquier b WHERE b.nombanquier = :nombanquier")
    , @NamedQuery(name = "Banquier.findByPrenombanquier", query = "SELECT b FROM Banquier b WHERE b.prenombanquier = :prenombanquier")
    , @NamedQuery(name = "Banquier.findBySexebanquier", query = "SELECT b FROM Banquier b WHERE b.sexebanquier = :sexebanquier")
    , @NamedQuery(name = "Banquier.findByTelephonebanquier", query = "SELECT b FROM Banquier b WHERE b.telephonebanquier = :telephonebanquier")})
public class Banquier implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "EMAILBANQUIER")
    private String emailbanquier;
    @Column(name = "IDENTIFIANTBANQUIER")
    private String identifiantbanquier;
    @Column(name = "MOTDEPASSEBANQUIER")
    private String motdepassebanquier;
    @Column(name = "NOMBANQUIER")
    private String nombanquier;
    @Column(name = "PRENOMBANQUIER")
    private String prenombanquier;
    @Column(name = "SEXEBANQUIER")
    private String sexebanquier;
    @Column(name = "TELEPHONEBANQUIER")
    private String telephonebanquier;
    @JoinColumn(name = "ID_AGENCE", referencedColumnName = "ID")
    @ManyToOne
    private Agence idAgence;
    @OneToMany(mappedBy = "idBanquier")
    private Collection<Client> clientCollection;

    public Banquier() {
    }

    public Banquier(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmailbanquier() {
        return emailbanquier;
    }

    public void setEmailbanquier(String emailbanquier) {
        this.emailbanquier = emailbanquier;
    }

    public String getIdentifiantbanquier() {
        return identifiantbanquier;
    }

    public void setIdentifiantbanquier(String identifiantbanquier) {
        this.identifiantbanquier = identifiantbanquier;
    }

    public String getMotdepassebanquier() {
        return motdepassebanquier;
    }

    public void setMotdepassebanquier(String motdepassebanquier) {
        this.motdepassebanquier = motdepassebanquier;
    }

    public String getNombanquier() {
        return nombanquier;
    }

    public void setNombanquier(String nombanquier) {
        this.nombanquier = nombanquier;
    }

    public String getPrenombanquier() {
        return prenombanquier;
    }

    public void setPrenombanquier(String prenombanquier) {
        this.prenombanquier = prenombanquier;
    }

    public String getSexebanquier() {
        return sexebanquier;
    }

    public void setSexebanquier(String sexebanquier) {
        this.sexebanquier = sexebanquier;
    }

    public String getTelephonebanquier() {
        return telephonebanquier;
    }

    public void setTelephonebanquier(String telephonebanquier) {
        this.telephonebanquier = telephonebanquier;
    }

    public Agence getIdAgence() {
        return idAgence;
    }

    public void setIdAgence(Agence idAgence) {
        this.idAgence = idAgence;
    }

    @XmlTransient
    public Collection<Client> getClientCollection() {
        return clientCollection;
    }

    public void setClientCollection(Collection<Client> clientCollection) {
        this.clientCollection = clientCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Banquier)) {
            return false;
        }
        Banquier other = (Banquier) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ws_entity.Banquier[ id=" + id + " ]";
    }
    
}
