/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws_entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author l.marchand
 */
@Entity
@Table(name = "SAUVEGARDECOMPREBENEFICIAIRE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sauvegardecomprebeneficiaire.findAll", query = "SELECT s FROM Sauvegardecomprebeneficiaire s")
    , @NamedQuery(name = "Sauvegardecomprebeneficiaire.findById", query = "SELECT s FROM Sauvegardecomprebeneficiaire s WHERE s.id = :id")
    , @NamedQuery(name = "Sauvegardecomprebeneficiaire.findByNomscb", query = "SELECT s FROM Sauvegardecomprebeneficiaire s WHERE s.nomscb = :nomscb")})
public class Sauvegardecomprebeneficiaire implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "NOMSCB")
    private String nomscb;
    @JoinColumn(name = "ID_CLIENTPROPRIETAIRE", referencedColumnName = "IDCLIENT")
    @ManyToOne
    private Client idClientproprietaire;
    @JoinColumn(name = "ID_COMPTEBENEFICIARE", referencedColumnName = "ID")
    @ManyToOne
    private Comptebancaire idComptebeneficiare;

    public Sauvegardecomprebeneficiaire() {
    }

    public Sauvegardecomprebeneficiaire(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomscb() {
        return nomscb;
    }

    public void setNomscb(String nomscb) {
        this.nomscb = nomscb;
    }

    public Client getIdClientproprietaire() {
        return idClientproprietaire;
    }

    public void setIdClientproprietaire(Client idClientproprietaire) {
        this.idClientproprietaire = idClientproprietaire;
    }

    public Comptebancaire getIdComptebeneficiare() {
        return idComptebeneficiare;
    }

    public void setIdComptebeneficiare(Comptebancaire idComptebeneficiare) {
        this.idComptebeneficiare = idComptebeneficiare;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sauvegardecomprebeneficiaire)) {
            return false;
        }
        Sauvegardecomprebeneficiaire other = (Sauvegardecomprebeneficiaire) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ws_entity.Sauvegardecomprebeneficiaire[ id=" + id + " ]";
    }
    
}
