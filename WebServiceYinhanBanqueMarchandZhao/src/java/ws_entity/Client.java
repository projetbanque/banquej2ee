/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws_entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author l.marchand
 */
@Entity
@Table(name = "CLIENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c")
    , @NamedQuery(name = "Client.findByIdclient", query = "SELECT c FROM Client c WHERE c.idclient = :idclient")
    , @NamedQuery(name = "Client.findByActif", query = "SELECT c FROM Client c WHERE c.actif = :actif")
    , @NamedQuery(name = "Client.findByCodepostal", query = "SELECT c FROM Client c WHERE c.codepostal = :codepostal")
    , @NamedQuery(name = "Client.findByComptedefaultnfc", query = "SELECT c FROM Client c WHERE c.comptedefaultnfc = :comptedefaultnfc")
    , @NamedQuery(name = "Client.findByDatedenaissance", query = "SELECT c FROM Client c WHERE c.datedenaissance = :datedenaissance")
    , @NamedQuery(name = "Client.findByEmail", query = "SELECT c FROM Client c WHERE c.email = :email")
    , @NamedQuery(name = "Client.findByIdentifiant", query = "SELECT c FROM Client c WHERE c.identifiant = :identifiant")
    , @NamedQuery(name = "Client.findByMotdepasse", query = "SELECT c FROM Client c WHERE c.motdepasse = :motdepasse")
    , @NamedQuery(name = "Client.findByNom", query = "SELECT c FROM Client c WHERE c.nom = :nom")
    , @NamedQuery(name = "Client.findByNumerorue", query = "SELECT c FROM Client c WHERE c.numerorue = :numerorue")
    , @NamedQuery(name = "Client.findByPays", query = "SELECT c FROM Client c WHERE c.pays = :pays")
    , @NamedQuery(name = "Client.findByPrenom", query = "SELECT c FROM Client c WHERE c.prenom = :prenom")
    , @NamedQuery(name = "Client.findByRue", query = "SELECT c FROM Client c WHERE c.rue = :rue")
    , @NamedQuery(name = "Client.findBySexe", query = "SELECT c FROM Client c WHERE c.sexe = :sexe")
    , @NamedQuery(name = "Client.findByTelephone", query = "SELECT c FROM Client c WHERE c.telephone = :telephone")
    , @NamedQuery(name = "Client.findByVille", query = "SELECT c FROM Client c WHERE c.ville = :ville")
    , @NamedQuery(name = "Client.login", query="SELECT c FROM Client c WHERE c.identifiant=:identifiant AND c.motdepasse=:motdepasse")})
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "IDCLIENT")
    private Long idclient;
    @Column(name = "ACTIF")
    private Short actif;
    @Column(name = "CODEPOSTAL")
    private String codepostal;
    @Column(name = "COMPTEDEFAULTNFC")
    private Integer comptedefaultnfc;
    @Column(name = "DATEDENAISSANCE")
    @Temporal(TemporalType.DATE)
    private Date datedenaissance;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "IDENTIFIANT")
    private String identifiant;
    @Column(name = "MOTDEPASSE")
    private String motdepasse;
    @Column(name = "NOM")
    private String nom;
    @Column(name = "NUMERORUE")
    private Integer numerorue;
    @Column(name = "PAYS")
    private String pays;
    @Column(name = "PRENOM")
    private String prenom;
    @Column(name = "RUE")
    private String rue;
    @Column(name = "SEXE")
    private String sexe;
    @Column(name = "TELEPHONE")
    private String telephone;
    @Column(name = "VILLE")
    private String ville;
    @OneToMany(mappedBy = "idClientproprietaire")
    private Collection<Sauvegardecomprebeneficiaire> sauvegardecomprebeneficiaireCollection;
    @OneToMany(mappedBy = "idClient")
    private Collection<Comptebancaire> comptebancaireCollection;
    @JoinColumn(name = "ID_BANQUIER", referencedColumnName = "ID")
    @ManyToOne
    private Banquier idBanquier;

    public Client() {
    }

    public Client(Long idclient) {
        this.idclient = idclient;
    }

    public Long getIdclient() {
        return idclient;
    }

    public void setIdclient(Long idclient) {
        this.idclient = idclient;
    }

    public Short getActif() {
        return actif;
    }

    public void setActif(Short actif) {
        this.actif = actif;
    }

    public String getCodepostal() {
        return codepostal;
    }

    public void setCodepostal(String codepostal) {
        this.codepostal = codepostal;
    }

    public Integer getComptedefaultnfc() {
        return comptedefaultnfc;
    }

    public void setComptedefaultnfc(Integer comptedefaultnfc) {
        this.comptedefaultnfc = comptedefaultnfc;
    }

    public Date getDatedenaissance() {
        return datedenaissance;
    }

    public void setDatedenaissance(Date datedenaissance) {
        this.datedenaissance = datedenaissance;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getMotdepasse() {
        return motdepasse;
    }

    public void setMotdepasse(String motdepasse) {
        this.motdepasse = motdepasse;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getNumerorue() {
        return numerorue;
    }

    public void setNumerorue(Integer numerorue) {
        this.numerorue = numerorue;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    @XmlTransient
    public Collection<Sauvegardecomprebeneficiaire> getSauvegardecomprebeneficiaireCollection() {
        return sauvegardecomprebeneficiaireCollection;
    }

    public void setSauvegardecomprebeneficiaireCollection(Collection<Sauvegardecomprebeneficiaire> sauvegardecomprebeneficiaireCollection) {
        this.sauvegardecomprebeneficiaireCollection = sauvegardecomprebeneficiaireCollection;
    }

    @XmlTransient
    public Collection<Comptebancaire> getComptebancaireCollection() {
        return comptebancaireCollection;
    }

    public void setComptebancaireCollection(Collection<Comptebancaire> comptebancaireCollection) {
        this.comptebancaireCollection = comptebancaireCollection;
    }

    public Banquier getIdBanquier() {
        return idBanquier;
    }

    public void setIdBanquier(Banquier idBanquier) {
        this.idBanquier = idBanquier;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idclient != null ? idclient.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Client)) {
            return false;
        }
        Client other = (Client) object;
        if ((this.idclient == null && other.idclient != null) || (this.idclient != null && !this.idclient.equals(other.idclient))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ws_entity.Client[ idclient=" + idclient + " ]";
    }
    
}
