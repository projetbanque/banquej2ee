/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws_entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author l.marchand
 */
@Entity
@Table(name = "COMPTEBANCAIRE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Comptebancaire.findAll", query = "SELECT c FROM Comptebancaire c")
    , @NamedQuery(name = "Comptebancaire.findById", query = "SELECT c FROM Comptebancaire c WHERE c.id = :id")
    , @NamedQuery(name = "Comptebancaire.findByActifcb", query = "SELECT c FROM Comptebancaire c WHERE c.actifcb = :actifcb")
    , @NamedQuery(name = "Comptebancaire.findByDatecreationcb", query = "SELECT c FROM Comptebancaire c WHERE c.datecreationcb = :datecreationcb")
    , @NamedQuery(name = "Comptebancaire.findByIbancb", query = "SELECT c FROM Comptebancaire c WHERE c.ibancb = :ibancb")
    , @NamedQuery(name = "Comptebancaire.findByNomcb", query = "SELECT c FROM Comptebancaire c WHERE c.nomcb = :nomcb")
    , @NamedQuery(name = "Comptebancaire.findBySoldecb", query = "SELECT c FROM Comptebancaire c WHERE c.soldecb = :soldecb")
    , @NamedQuery(name = "Comptebancaire.findByTransactionmaxcb", query = "SELECT c FROM Comptebancaire c WHERE c.transactionmaxcb = :transactionmaxcb")
    })
public class Comptebancaire implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "ACTIFCB")
    private Short actifcb;
    @Column(name = "DATECREATIONCB")
    @Temporal(TemporalType.DATE)
    private Date datecreationcb;
    @Column(name = "IBANCB")
    private String ibancb;
    @Column(name = "NOMCB")
    private String nomcb;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SOLDECB")
    private Double soldecb;
    @Column(name = "TRANSACTIONMAXCB")
    private Double transactionmaxcb;
    @OneToMany(mappedBy = "idComptebeneficiare")
    private Collection<Sauvegardecomprebeneficiaire> sauvegardecomprebeneficiaireCollection;
    @JoinColumn(name = "ID_CLIENT", referencedColumnName = "IDCLIENT")
    @ManyToOne
    private Client idClient;
    @JoinColumn(name = "ID_TYPEDECOMPTE", referencedColumnName = "ID")
    @ManyToOne
    private Typedecompte idTypedecompte;
    @OneToMany(mappedBy = "idComptecredit")
    private Collection<Transactionargent> transactionargentCollection;
    @OneToMany(mappedBy = "idComptedebit")
    private Collection<Transactionargent> transactionargentCollection1;

    public Comptebancaire() {
    }

    public Comptebancaire(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Short getActifcb() {
        return actifcb;
    }

    public void setActifcb(Short actifcb) {
        this.actifcb = actifcb;
    }

    public Date getDatecreationcb() {
        return datecreationcb;
    }

    public void setDatecreationcb(Date datecreationcb) {
        this.datecreationcb = datecreationcb;
    }

    public String getIbancb() {
        return ibancb;
    }

    public void setIbancb(String ibancb) {
        this.ibancb = ibancb;
    }

    public String getNomcb() {
        return nomcb;
    }

    public void setNomcb(String nomcb) {
        this.nomcb = nomcb;
    }

    public Double getSoldecb() {
        return soldecb;
    }

    public void setSoldecb(Double soldecb) {
        this.soldecb = soldecb;
    }

    public Double getTransactionmaxcb() {
        return transactionmaxcb;
    }

    public void setTransactionmaxcb(Double transactionmaxcb) {
        this.transactionmaxcb = transactionmaxcb;
    }

    @XmlTransient
    public Collection<Sauvegardecomprebeneficiaire> getSauvegardecomprebeneficiaireCollection() {
        return sauvegardecomprebeneficiaireCollection;
    }

    public void setSauvegardecomprebeneficiaireCollection(Collection<Sauvegardecomprebeneficiaire> sauvegardecomprebeneficiaireCollection) {
        this.sauvegardecomprebeneficiaireCollection = sauvegardecomprebeneficiaireCollection;
    }

    public Client getIdClient() {
        return idClient;
    }

    public void setIdClient(Client idClient) {
        this.idClient = idClient;
    }

    public Typedecompte getIdTypedecompte() {
        return idTypedecompte;
    }

    public void setIdTypedecompte(Typedecompte idTypedecompte) {
        this.idTypedecompte = idTypedecompte;
    }

    @XmlTransient
    public Collection<Transactionargent> getTransactionargentCollection() {
        return transactionargentCollection;
    }

    public void setTransactionargentCollection(Collection<Transactionargent> transactionargentCollection) {
        this.transactionargentCollection = transactionargentCollection;
    }

    @XmlTransient
    public Collection<Transactionargent> getTransactionargentCollection1() {
        return transactionargentCollection1;
    }

    public void setTransactionargentCollection1(Collection<Transactionargent> transactionargentCollection1) {
        this.transactionargentCollection1 = transactionargentCollection1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Comptebancaire)) {
            return false;
        }
        Comptebancaire other = (Comptebancaire) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ws_entity.Comptebancaire[ id=" + id + " ]";
    }
    
}
