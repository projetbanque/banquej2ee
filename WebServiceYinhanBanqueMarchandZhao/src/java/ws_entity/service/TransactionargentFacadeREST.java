/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws_entity.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import ws_entity.Client;
import ws_entity.Comptebancaire;
import ws_entity.Transactionargent;

/**
 *
 * @author l.marchand
 */
@Stateless
@Path("transactions")
public class TransactionargentFacadeREST extends AbstractFacade<Transactionargent> {

    @PersistenceContext(unitName = "WebServiceYinhanBanqueMarchandZhaoPU")
    private EntityManager em;

    public TransactionargentFacadeREST() {
        super(Transactionargent.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Transactionargent entity) {
        Comptebancaire credit = em.find(Comptebancaire.class, entity.getIdComptecredit().getId());
        Comptebancaire debit = em.find(Comptebancaire.class, entity.getIdComptedebit().getId());
        entity.setIdComptecredit(credit);
        entity.setIdComptedebit(debit);
        super.create(entity);
        
        //update compte
        credit.setSoldecb(credit.getSoldecb()+entity.getSommetransferee());
        debit.setSoldecb(debit.getSoldecb()-entity.getSommetransferee());
        entity.setStatus(2);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Long id, Transactionargent entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Transactionargent find(@PathParam("id") Long id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Transactionargent> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Transactionargent> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("transactionclient/{idf}/{mdp}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
     public List<Transactionargent> compteClient(@PathParam("idf") String identifiant,@PathParam("mdp") String motdepasse){
         
        Query qclient = getEntityManager().createQuery("SELECT c FROM Client c WHERE c.identifiant=:identifiant AND c.motdepasse=:motdepasse");
        Client client = (Client) qclient.setParameter("identifiant", identifiant).setParameter("motdepasse", motdepasse).getSingleResult();
        
        Query q = getEntityManager().createQuery("SELECT t FROM Transactionargent t WHERE (t.idComptedebit.idClient=:client or t.idComptecredit.idClient=:client) ORDER BY t.datetransfert DESC");
        q.setParameter("client", client);
        
        return q.getResultList();
    }
    
     /*
    @POST
    @Path("transactionclient/{idf}/{mdp}")
    @Consumes("text/plain")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
     public void newTransactionClient(@PathParam("idf") String identifiant,@PathParam("mdp") String motdepasse,String ibanDebit,String ibanCredit,String sommes,String libelle){
         
        try {
            Query qclient = getEntityManager().createQuery("SELECT c FROM Client c WHERE c.identifiant=:identifiant AND c.motdepasse=:motdepasse");
            Client client = (Client) qclient.setParameter("identifiant", identifiant).setParameter("motdepasse", motdepasse).getSingleResult();
            
            //permet de proteger l'acces au compte en verifiant si c'est le proprietaire du compte qui fait le virement
            Query queryDebit = getEntityManager().createQuery("SELECT cb FROM Comptebancaire cb WHERE cb.ibancb =:ibancb AND cb.idClient=:idClient").setParameter("ibancb", ibanDebit).setParameter("idClient", client);
            
            
            Query queryCredit = getEntityManager().createNamedQuery("Comptebancaire.findByIbancb").setParameter("ibancb", ibanCredit);
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-dd-MM");
            //Date datetransfert, String libelle, Double sommetransferee, Integer status, Comptebancaire idComptecredit, Comptebancaire idComptedebit
            create(new Transactionargent(sdf.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-dd-MM"))),libelle,Double.parseDouble(sommes),2,(Comptebancaire)queryDebit.getSingleResult(),(Comptebancaire)queryCredit.getSingleResult()));
            
        } catch (ParseException ex) {
            Logger.getLogger(TransactionargentFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     */
}
