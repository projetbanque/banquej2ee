/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws_entity.service;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import ws_entity.Client;
import ws_entity.Sauvegardecomprebeneficiaire;

/**
 *
 * @author l.marchand
 */
@Stateless
@Path("ws_entity.sauvegardecomprebeneficiaire")
public class SauvegardecomprebeneficiaireFacadeREST extends AbstractFacade<Sauvegardecomprebeneficiaire> {

    @PersistenceContext(unitName = "WebServiceYinhanBanqueMarchandZhaoPU")
    private EntityManager em;

    public SauvegardecomprebeneficiaireFacadeREST() {
        super(Sauvegardecomprebeneficiaire.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Sauvegardecomprebeneficiaire entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Long id, Sauvegardecomprebeneficiaire entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Sauvegardecomprebeneficiaire find(@PathParam("id") Long id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Sauvegardecomprebeneficiaire> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Sauvegardecomprebeneficiaire> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("comptebeneficiaireclient/{idf}/{mdp}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
     public List<Sauvegardecomprebeneficiaire> compteClient(@PathParam("idf") String identifiant,@PathParam("mdp") String motdepasse){
         
        Query qclient = getEntityManager().createQuery("SELECT c FROM Client c WHERE c.identifiant=:identifiant AND c.motdepasse=:motdepasse");
        Client client = (Client) qclient.setParameter("identifiant", identifiant).setParameter("motdepasse", motdepasse).getSingleResult();
        
        Query q = getEntityManager().createQuery("SELECT s FROM Sauvegardecomprebeneficiaire s WHERE s.idClientproprietaire=:client");
        q.setParameter("client", client);
        
        return q.getResultList();
    }
    
}
