/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws_entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author l.marchand
 */
@Entity
@Table(name = "TYPEDECOMPTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Typedecompte.findAll", query = "SELECT t FROM Typedecompte t")
    , @NamedQuery(name = "Typedecompte.findById", query = "SELECT t FROM Typedecompte t WHERE t.id = :id")
    , @NamedQuery(name = "Typedecompte.findByNfcautorise", query = "SELECT t FROM Typedecompte t WHERE t.nfcautorise = :nfcautorise")
    , @NamedQuery(name = "Typedecompte.findByNomtc", query = "SELECT t FROM Typedecompte t WHERE t.nomtc = :nomtc")
    , @NamedQuery(name = "Typedecompte.findByPlafond", query = "SELECT t FROM Typedecompte t WHERE t.plafond = :plafond")
    , @NamedQuery(name = "Typedecompte.findByTransactionmaxdefault", query = "SELECT t FROM Typedecompte t WHERE t.transactionmaxdefault = :transactionmaxdefault")})
public class Typedecompte implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "NFCAUTORISE")
    private Short nfcautorise;
    @Column(name = "NOMTC")
    private String nomtc;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PLAFOND")
    private Double plafond;
    @Column(name = "TRANSACTIONMAXDEFAULT")
    private Double transactionmaxdefault;
    @OneToMany(mappedBy = "idTypedecompte")
    private Collection<Comptebancaire> comptebancaireCollection;

    public Typedecompte() {
    }

    public Typedecompte(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Short getNfcautorise() {
        return nfcautorise;
    }

    public void setNfcautorise(Short nfcautorise) {
        this.nfcautorise = nfcautorise;
    }

    public String getNomtc() {
        return nomtc;
    }

    public void setNomtc(String nomtc) {
        this.nomtc = nomtc;
    }

    public Double getPlafond() {
        return plafond;
    }

    public void setPlafond(Double plafond) {
        this.plafond = plafond;
    }

    public Double getTransactionmaxdefault() {
        return transactionmaxdefault;
    }

    public void setTransactionmaxdefault(Double transactionmaxdefault) {
        this.transactionmaxdefault = transactionmaxdefault;
    }

    @XmlTransient
    public Collection<Comptebancaire> getComptebancaireCollection() {
        return comptebancaireCollection;
    }

    public void setComptebancaireCollection(Collection<Comptebancaire> comptebancaireCollection) {
        this.comptebancaireCollection = comptebancaireCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Typedecompte)) {
            return false;
        }
        Typedecompte other = (Typedecompte) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ws_entity.Typedecompte[ id=" + id + " ]";
    }
    
}
