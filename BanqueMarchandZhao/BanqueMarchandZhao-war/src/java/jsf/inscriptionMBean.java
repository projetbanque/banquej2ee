/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Banquier;
import entity.Client;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.persistence.Column;
import javax.persistence.Temporal;
import session.GestionnaireDeBanquier;
import session.GestionnaireDeClient;

/**
 *
 * @author l.marchand
 */
@Named(value = "inscriptionMBean")
@ViewScoped
public class inscriptionMBean implements Serializable {
    @EJB
    GestionnaireDeClient gestionnaireDeClient;
    @EJB
    GestionnaireDeBanquier gestionnaireDeBanquier;
     
    private Client newClient;
    private String nom;
    private String prenom;
    private String ville;
    private String codePostal;
    private String rue;
    private int numeroRue;
    private String pays;
    private String email;
    private String telephone;
    private String motDePasse;
    private String identifiant;
    private String sexe;
    private Date dateDeNaissance;
    
    private Banquier banquier;
    private List<Banquier> banquiers;

    public Client getNewClient() {
        return newClient;
    }

    public void setNewClient(Client newClient) {
        this.newClient = newClient;
    }
    /**
     * Creates a new instance of inscriptionMBean
     */
    public inscriptionMBean() {
    }
    
    public String inscrireClient(){
        
        Banquier randomBanquier = gestionnaireDeBanquier.getRandomBanquier();
        newClient = new Client(prenom,nom,ville,codePostal,rue,numeroRue,pays,email,telephone,motDePasse,identifiant,sexe,true,dateDeNaissance,0,randomBanquier);
        gestionnaireDeClient.creerClient(newClient);

        return "acceuil.xhtml";
            
    }
    
    public String inscrireClientParBanquier(Banquier banquier){        
        newClient = new Client(prenom,nom,ville,codePostal,rue,numeroRue,pays,email,telephone,motDePasse,identifiant,sexe,true,dateDeNaissance,0,banquier);
        gestionnaireDeClient.creerClient(newClient);

        return ("mesClients_agent.xhtml");            
    }

    public GestionnaireDeClient getGestionnaireDeClient() {
        return gestionnaireDeClient;
    }

    public void setGestionnaireDeClient(GestionnaireDeClient gestionnaireDeClient) {
        this.gestionnaireDeClient = gestionnaireDeClient;
    }

    public GestionnaireDeBanquier getGestionnaireDeBanquier() {
        return gestionnaireDeBanquier;
    }

    public void setGestionnaireDeBanquier(GestionnaireDeBanquier gestionnaireDeBanquier) {
        this.gestionnaireDeBanquier = gestionnaireDeBanquier;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public int getNumeroRue() {
        return numeroRue;
    }

    public void setNumeroRue(int numeroRue) {
        this.numeroRue = numeroRue;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public Date getDateDeNaissance() {
        return dateDeNaissance;
    }

    public void setDateDeNaissance(Date dateDeNaissance) {
        this.dateDeNaissance = dateDeNaissance;
    }

    public Banquier getBanquier() {
        return banquier;
    }

    public void setBanquier(Banquier banquier) {
        this.banquier = banquier;
    }

    public List<Banquier> getBanquiers() {
        return gestionnaireDeBanquier.getAllBanquier();
    }

    public void setBanquiers(List<Banquier> banquiers) {
        this.banquiers = banquiers;
    }
    
    
    
}
