/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Banquier;
import entity.Client;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import session.GestionnaireDeBanquier;
import session.GestionnaireDeClient;

/**
 *
 * @author l.marchand
 */
@Named(value = "loginMBean_agent")
@SessionScoped
public class loginMBean_agent implements Serializable {

    @EJB
    GestionnaireDeBanquier gestionnaireDeBanquier;
    @EJB
    GestionnaireDeClient gestionnaireDeClient;
    
    private Banquier banquier;
    private String identifiant;
    private String motDePasse;
    private boolean connected = false;
    
    //pour quand le banquier regarde la page d'un client en particulier
    private Client selectedClient;
    
    /**
     * Creates a new instance of loginMBean
     */
    public loginMBean_agent() {
    }
    
    
    
    public String verifierIdentification() {        
        try{
            Banquier b = gestionnaireDeBanquier.verifierIdentificationBanquier(identifiant, motDePasse);

            if (b!=null){
                banquier=b;
                connected=true;
            }
            if (connected) {
                return ("mesClients_agent.xhtml");
            }else{
                return "notlogin";
        }
         }catch(Exception e){
            return "notlogin";
         }
         
    }
    
    public void deconnexion() {
        connected = false;
    }
    
     public String updateValues(){
        gestionnaireDeBanquier.update(banquier);
        return ("monProfil_agent.xhtml");
        
    }
     
    public String updateValuesClient(){
        gestionnaireDeClient.update(selectedClient);
        return ("monClient_agent.xhtml");
        
    }
     
     public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public Banquier getBanquier() {
        return banquier;
    }

    public void setBanquier(Banquier banquier) {
        this.banquier = banquier;
    }

    public String getLogin() {
        return identifiant;
    }

    public void setLogin(String login) {
        this.identifiant = login;
    }
    
    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String login) {
        this.identifiant = login;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public Client getSelectedClient() {
        return selectedClient;
    }

    public void setSelectedClient(Client selectedClient) {
        this.selectedClient = selectedClient;
    }
    
    

    
}
