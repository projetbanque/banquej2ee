/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Client;
import entity.CompteBancaire;
import entity.SauvegardeCompteBeneficiaire;
import entity.TransactionArgent;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import session.GestionnaireDeCompteBancaire;
import session.GestionnaireDeSauvegardeCompteBeneficiaire;
import session.GestionnaireDeTransactionArgent;

/**
 *
 * @author l.marchand
 */
@Named(value = "virementMBean")
@SessionScoped
public class virementMBean implements Serializable {

    @EJB
    GestionnaireDeCompteBancaire gestionnaireDeCompteBancaire;
    @EJB
    GestionnaireDeSauvegardeCompteBeneficiaire gestionnaireDeSauvegardeCompteBeneficiaire;
    @EJB
    GestionnaireDeTransactionArgent gestionnaireDeTransactionArgent;
    
    private CompteBancaire selectedCompte;
    private CompteBancaire selectedCompteBeneficiaire;
    private SauvegardeCompteBeneficiaire newSauvegardeCompteBeneficiaire;
    private String nomNewCompteBeneficiaire;
    private String iBANNewCompteBeneficiaire;
    private String libelle;
    private Float sommes;
    private SauvegardeCompteBeneficiaire selectedSauvegardeCompteBeneficiaire;

   
    /**
     * Creates a new instance of virementMBean
     */
    public virementMBean() {
    }
    
    public String goToSelectCompteBeneficiaire(){
        if (selectedCompte!=null){
            return ("virement_client_comptebeneficiaire_client.xhtml");
        }
        else{
            return "nothing selected";
        }
    }
    public String goToSelectMontant(){
        if (selectedSauvegardeCompteBeneficiaire!=null){
            return ("virement_montant_client.xhtml");
        }
        else{
            return "nothing selected";
        }
    }
    
    public String saveTransfert(){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-dd-MM");
            //TransactionArgent(Float sommeTransferee, String libelle, int status, Date dateTransfert, CompteBancaire compteDebit, CompteBancaire compteCredit)
            gestionnaireDeTransactionArgent.creerTransactionArgent(new TransactionArgent(this.sommes,this.libelle,2,sdf.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-dd-MM"))),this.selectedCompte,this.selectedSauvegardeCompteBeneficiaire.getCompteBeneficiaire()));
         
           
            //ajuster argent donneur
            gestionnaireDeCompteBancaire.retirerArgent(selectedCompte, sommes);
            
            //ajuster argent receveur
            gestionnaireDeCompteBancaire.ajouterArgent(selectedSauvegardeCompteBeneficiaire.getCompteBeneficiaire(), sommes);
            
            return ("mesComptes_client.xhtml");
        } catch (ParseException ex) {
            Logger.getLogger(virementMBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ("mesComptes_client.xhtml");
    }
    
    public String addNewCompteBeneficiaire(Client client){
        //public SauvegardeCompteBeneficiaire(CompteBancaire compteBeneficiaire, Client clientProprietaire, String nom)
        
        CompteBancaire comptebeneficiaire = gestionnaireDeCompteBancaire.getCompte(iBANNewCompteBeneficiaire);
        gestionnaireDeSauvegardeCompteBeneficiaire.creerSauvegardeCompteBeneficiaire(new SauvegardeCompteBeneficiaire(comptebeneficiaire ,client,nomNewCompteBeneficiaire));
        
        return ("virement_client_comptebeneficiaire_client.xhtml");
    }
    
    
    public List<CompteBancaire> getCompteBancaireClient(String identifiant) { 
        return gestionnaireDeCompteBancaire.getAllComptesOfClient(identifiant); 
    }
    
    public List<CompteBancaire> getActiveCompteBancaireClient(String identifiant) {  
        return gestionnaireDeCompteBancaire.getAllActiveComptesOfClient(identifiant); 
    }
    
    public List<SauvegardeCompteBeneficiaire> getCompteBancaireBeneficiaire(Client client) { 
        return gestionnaireDeSauvegardeCompteBeneficiaire.getAllSauvegardeCompteBeneficiaireOfClient(client); 
    }
    
    public CompteBancaire getSelectedCompte() {
        return selectedCompte;
    }

    public void setSelectedCompte(CompteBancaire selectedCompte) {
        this.selectedCompte = selectedCompte;
    }
    public CompteBancaire getSelectedCompteBeneficiaire() {
        return selectedCompteBeneficiaire;
    }
        
    public void setSelectedSauvegardeCompteBeneficiaire(SauvegardeCompteBeneficiaire selectedSauvegardeCompteBeneficiaire) {
        this.selectedSauvegardeCompteBeneficiaire = selectedSauvegardeCompteBeneficiaire;
    }
    
     public SauvegardeCompteBeneficiaire getSelectedSauvegardeCompteBeneficiaire() {
        return selectedSauvegardeCompteBeneficiaire;
    }

    public void setSelectedCompteBeneficiaire(CompteBancaire selectedCompteBeneficiaire) {
        this.selectedCompteBeneficiaire = selectedCompteBeneficiaire;
    }
    
    public SauvegardeCompteBeneficiaire getNewCompteBeneficiaire() {
        return newSauvegardeCompteBeneficiaire;
    }

    public void setNewSauvegardeCompteBeneficiaire(SauvegardeCompteBeneficiaire newSauvegardeCompteBeneficiaire) {
        this.newSauvegardeCompteBeneficiaire = newSauvegardeCompteBeneficiaire;
    }

    public String getNomNewCompteBeneficiaire() {
        return nomNewCompteBeneficiaire;
    }

    public void setNomNewCompteBeneficiaire(String nomNewCompteBeneficiaire) {
        this.nomNewCompteBeneficiaire = nomNewCompteBeneficiaire;
    }

    public String getiBANNewCompteBeneficiaire() {
        return iBANNewCompteBeneficiaire;
    }

    public void setiBANNewCompteBeneficiaire(String iBANNewCompteBeneficiaire) {
        this.iBANNewCompteBeneficiaire = iBANNewCompteBeneficiaire;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Float getSommes() {
        return sommes;
    }

    public void setSommes(Float sommes) {
        this.sommes = sommes;
    }

    
    
}
