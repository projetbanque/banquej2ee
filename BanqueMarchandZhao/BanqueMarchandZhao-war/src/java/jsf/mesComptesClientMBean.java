/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;  
  
import entity.CompteBancaire;
import entity.TransactionArgent;
import javax.ejb.EJB;  
import javax.inject.Named;  
import javax.faces.view.ViewScoped;  
import java.io.Serializable;  
import java.util.List;  
import javax.ejb.SessionBean;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import session.GestionnaireDeCompteBancaire;  
import session.GestionnaireDeTransactionArgent;
  
@Named(value = "mesComptesClientMBean")  
@ViewScoped  
public class mesComptesClientMBean implements Serializable {
    
    private List<CompteBancaire> compteBancaireList;  
    private CompteBancaire selectedCompte;

   
    @EJB  
    private GestionnaireDeCompteBancaire gestionnaireDeCompteBancaire;
    @EJB  
    private GestionnaireDeTransactionArgent gestionnaireDeTransactionArgent;
  
    public mesComptesClientMBean() { 
    }
  
    /** 
     * Renvoie la liste des comptes du client pour affichage dans une DataTable 
     * @param identifiant
     * @return 
     */  
    public List<CompteBancaire> getCompteBancaireClient(String identifiant) {  
        //String identifiant = sessionData.getLogin();
        //System.out.println(identifiant);
        return gestionnaireDeCompteBancaire.getAllComptesOfClient(identifiant); 
    }
    
    public List<CompteBancaire> getActiveCompteBancaireClient(String identifiant) {  
        //String identifiant = sessionData.getLogin();
        //System.out.println(identifiant);
        return gestionnaireDeCompteBancaire.getAllActiveComptesOfClient(identifiant); 
    }
    
    public List<TransactionArgent> getTransactionClient(String identifiant){          
        return gestionnaireDeTransactionArgent.getAllTransactionOfClient(identifiant);
    }
    
    public List<TransactionArgent> getTransactionClientOfCompte(String identifiant) {  
        return gestionnaireDeTransactionArgent.getTransactionOfClientOfCompte(identifiant,this.selectedCompte.getiBANCB()); 
                
    }
    
    public void supprimerCompte(CompteBancaire compte){
        gestionnaireDeCompteBancaire.delete(compte);
    }
    
    public void desactiverCompte(CompteBancaire compte){
        gestionnaireDeCompteBancaire.updateIsActif(compte);
    }
    public void activerCompte(CompteBancaire compte){
        gestionnaireDeCompteBancaire.updateIsNotActif(compte);
    }
    
  
    /** 
     * Action handler - appelé lorsque l'utilisateur sélectionne une ligne dans 
     * la DataTable pour voir les détails 
     */  
    public String showDetails(int compteId) {  
        return "CompteDetails?idCompte=" + compteId;
    }
    
    public CompteBancaire getSelectedCompte() {
        return selectedCompte;
    }

    public void setSelectedCompte(CompteBancaire selectedCompte) {
        this.selectedCompte = selectedCompte;
    }
    
   
}  
