/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Client;
import entity.TransactionArgent;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import session.GestionnaireDeClient;

/**
 *
 * @author l.marchand
 */
@Named(value = "loginMBean")
@SessionScoped
public class loginMBean implements Serializable {
    
    @EJB
    GestionnaireDeClient gestionnaireDeClient;
    private Client client;
    private String identifiant;
    private String motDePasse;
    private boolean connected = false;
    /**
     * Creates a new instance of loginMBean
     */
    public loginMBean() {
    }
    
     public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }
    
    public Client getClient() {
        return client;
    }
    

    public void setClient(Client client) {
        this.client = client;
    }

    public String getLogin() {
        return identifiant;
    }

    public void setLogin(String login) {
        this.identifiant = login;
    }
    
    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String login) {
        this.identifiant = login;
    }

    public String getPassword() {
        return motDePasse;
    }

    public void setPassword(String password) {
        this.motDePasse = password;
    }
    
    public String verifierIdentification() {        
        
        try{
            Client c = gestionnaireDeClient.verifierIdentificationClient(identifiant, motDePasse);
            if (c!=null){
            client=c;
            connected=true;
        }
        if (connected) {
            return ("mesComptes_client.xhtml");
        }else{
            return "notlogin";
        }
        }catch(Exception e){
            return "wrong identifiant/password";
        }
        
        
    }
    
    public void deconnexion() {
        connected = false;
    }
    
     public String updateValues(){
        gestionnaireDeClient.update(client);
        return ("monProfil_client.xhtml");
        
    }
    
    
}
