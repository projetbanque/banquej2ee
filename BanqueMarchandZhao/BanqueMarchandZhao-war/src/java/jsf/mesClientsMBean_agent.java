/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import entity.Client;
import entity.CompteBancaire;
import entity.TypeDeCompte;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import session.GestionnaireDeClient;
import session.GestionnaireDeCompteBancaire;
import session.GestionnaireDeTypeDeCompte;

/**
 *
 * @author l.marchand
 */
@Named(value = "mesClientsMBean_agent")
@ViewScoped
public class mesClientsMBean_agent implements Serializable {
    @EJB
    GestionnaireDeClient gestionnaireDeClient;
    @EJB
    GestionnaireDeCompteBancaire gestionnaireDeCompteBancaire;
    @EJB
    GestionnaireDeTypeDeCompte gestionnaireDeTypeDeCompte;
    List<Client> clientList;
    Client selectedClient;
    CompteBancaire newCompte;
    String iBAN;
    String nomCompte; 
    float solde; 
    float transactionMax;
    Client client; 
    TypeDeCompte typeDeCompte;
    List<TypeDeCompte> typeDeComptes;


    /**
     * Creates a new instance of mesClientsMBean_agent
     */
    public mesClientsMBean_agent() {
    }
    
    public String ajouterCompte(Client client){
        try {
            //String iBAN, String nom, float solde, Date dateCreation, float transactionMax,
            //Boolean actif, Client client, TypeDeCompte typeDeCompte
            typeDeCompte = gestionnaireDeTypeDeCompte.getTypeDeCompte("DEFAULT");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-dd-MM");
            gestionnaireDeCompteBancaire.creerCompte(new CompteBancaire(iBAN,nomCompte,solde,sdf.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-dd-MM"))),transactionMax,true,client ,typeDeCompte ));
            
            return ("monClient_agent.xhtml");
        } catch (ParseException ex) {
            Logger.getLogger(mesClientsMBean_agent.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
    public List<Client> getClientList(String identifiant) {
        return gestionnaireDeClient.getAllClientOfAgent(identifiant);
    }

    public void setClientList(List<Client> clientList) {
        this.clientList = clientList;
    }
    
    public String goToClient(){
            return "monClient_agent.xhtml";
    }

    public Client getSelectedClient() {
        return selectedClient;
    }

    public void setSelectedClient(Client selectedClient) {
        this.selectedClient = selectedClient;
    }

    public CompteBancaire getNewCompte() {
        return newCompte;
    }

    public void setNewCompte(CompteBancaire newCompte) {
        this.newCompte = newCompte;
    }

    public String getiBAN() {
        return iBAN;
    }

    public void setiBAN(String iBAN) {
        this.iBAN = iBAN;
    }

    public String getNomCompte() {
        return nomCompte;
    }

    public void setNomCompte(String nomCompte) {
        this.nomCompte = nomCompte;
    }

    public float getSolde() {
        return solde;
    }

    public void setSolde(float solde) {
        this.solde = solde;
    }

    public float getTransactionMax() {
        return transactionMax;
    }

    public void setTransactionMax(float transactionMax) {
        this.transactionMax = transactionMax;
    }

    public TypeDeCompte getTypeDeCompte() {
        return typeDeCompte;
    }

    public void setTypeDeCompte(TypeDeCompte typeDeCompte) {
        this.typeDeCompte = typeDeCompte;
    }

    public List<TypeDeCompte> getTypeDeComptes() {
        return gestionnaireDeTypeDeCompte.getAllTypeDeCompte();
    }

    public void setTypeDeComptes(List<TypeDeCompte> typeDeComptes) {
        this.typeDeComptes = typeDeComptes;
    }

    
    
}
