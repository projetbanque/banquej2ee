/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Client;
import entity.CompteBancaire;
import entity.TypeDeCompte;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author l.marchand
 */
@Stateless
@LocalBean
public class GestionnaireDeTypeDeCompte {

   @PersistenceContext(unitName = "BanqueMarchandZhao-ejbPU")
    private EntityManager em;



    public void creerTypeDeCompte(TypeDeCompte t) {
        persist(t);
    }

    public List<TypeDeCompte> getAllTypeDeCompte() {
        Query query = em.createNamedQuery("TypeDeCompte.findAll");  
        return query.getResultList();
    }
    
    public TypeDeCompte getTypeDeCompte(String nom) {
        Query query = em.createNamedQuery("TypeDeCompte.findByNom").setParameter("nom", nom);  
        return (TypeDeCompte)query.getSingleResult();
    }
    
    


    
    public void creerTypeDeCompteTest() { 
    //TypeDeCompte(Boolean NFCautorise, float plafond, String nom, float transactionMaxDefault)
           
            creerTypeDeCompte(new TypeDeCompte(true,10000,"CompteCourant",1000));
            creerTypeDeCompte(new TypeDeCompte(false,10000,"LivretA",1000));
            creerTypeDeCompte(new TypeDeCompte(false,10000,"PEL",10000));
            creerTypeDeCompte(new TypeDeCompte(true,-1,"DEFAULT",-1));
            
}  

    public void persist(Object object) {
        em.persist(object);
    }
    
    public void update(TypeDeCompte t) {
        em.merge(t);
    }
}
