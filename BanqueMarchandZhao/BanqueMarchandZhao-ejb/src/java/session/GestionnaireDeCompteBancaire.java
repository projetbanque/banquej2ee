/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Client;
import entity.CompteBancaire;
import entity.TypeDeCompte;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author l.marchand
 */
@Stateless
@LocalBean
public class GestionnaireDeCompteBancaire {

    @PersistenceContext(unitName = "BanqueMarchandZhao-ejbPU")
    private EntityManager em;



    public void creerCompte(CompteBancaire c) {
        persist(c);
    }
    
    public void retirerArgent(CompteBancaire compte,Float sommes) {
        compte.setSoldeCB(compte.getSoldeCB()-sommes);
        update(compte);
    }
    
    public void ajouterArgent(CompteBancaire compte,Float sommes) {
        compte.setSoldeCB(compte.getSoldeCB()+sommes);
        update(compte);
    }

    public List<CompteBancaire> getAllComptes() {
        Query query = em.createNamedQuery("CompteBancaire.findAll");  
        return query.getResultList();
    }
    
    public CompteBancaire getCompte(String iBAN) {
        Query query = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", iBAN); 
        return (CompteBancaire) query.getSingleResult();
    }
    
    public List<CompteBancaire> getAllComptesOfClient(String identifiant) {
        Query queryClient = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", identifiant);   
        Query query = em.createNamedQuery("CompteBancaire.findByClient").setParameter("client",(Client)queryClient.getSingleResult());  
        return query.getResultList();
    }
    
    public List<CompteBancaire> getAllActiveComptesOfClient(String identifiant) {
        Query queryClient = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", identifiant);   
        Query query = em.createQuery("SELECT c FROM CompteBancaire c WHERE c.client = :client AND c.actifCB=true").setParameter("client",(Client)queryClient.getSingleResult());  
        return query.getResultList();
    }


    
    public void creerComptesTest() { 
        try {
            //String iBAN, String nom, float solde, Date dateCreation, float transactionMax,
            //Boolean actif, Client client, TypeDeCompte typeDeCompte
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            
            Query queryTypeDeCompte = em.createNamedQuery("TypeDeCompte.findByNom").setParameter("nom", "CompteCourant");
            TypeDeCompte typeCompteCourant=(TypeDeCompte)queryTypeDeCompte.getSingleResult();
            queryTypeDeCompte = em.createNamedQuery("TypeDeCompte.findByNom").setParameter("nom", "LivretA");
            TypeDeCompte typeLivretA=(TypeDeCompte)queryTypeDeCompte.getSingleResult();
            queryTypeDeCompte = em.createNamedQuery("TypeDeCompte.findByNom").setParameter("nom", "PEL");
            TypeDeCompte typePEL=(TypeDeCompte)queryTypeDeCompte.getSingleResult();
            
            Query query = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", "jojo");    

            creerCompte(new CompteBancaire("FR7630004015870002601171220","MonCompteCourant",1000,sdf.parse("1960-02-10"),500,true,(Client)query.getSingleResult() ,typeCompteCourant ));
            
             query = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", "toto");      
             creerCompte(new CompteBancaire("FR7630004015870002601171221","CompteCourant",1000,sdf.parse("1960-02-10"),500,true,(Client)query.getSingleResult() ,typeCompteCourant ));
             creerCompte(new CompteBancaire("FR7630004015870002601171222","ArgentGrandParent",5000,sdf.parse("1960-02-10"),1000,true,(Client)query.getSingleResult() ,typeLivretA ));
             
            query = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", "momo"); 
             creerCompte(new CompteBancaire("FR7630004015870002601171223","CompteCourant",1000,sdf.parse("1960-02-10"),500,true,(Client)query.getSingleResult() ,typeCompteCourant ));
             creerCompte(new CompteBancaire("FR7630004015870002601171224","LivretA",5000,sdf.parse("1960-02-10"),1000,true,(Client)query.getSingleResult() ,typeLivretA ));
             creerCompte(new CompteBancaire("FR7630004015870002601171225","CompteMaison",50000,sdf.parse("1960-02-10"),0,true,(Client)query.getSingleResult() ,typePEL ));
             
             query = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", "coco"); 
             creerCompte(new CompteBancaire("FR7630004015870002601171226","PourCarteBleu",100,sdf.parse("1960-02-10"),500,true,(Client)query.getSingleResult() ,typeCompteCourant ));
             
            query = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", "vovo"); 
             creerCompte(new CompteBancaire("FR7630004015870002601171227","CompteA",1000,sdf.parse("1960-02-10"),500,true,(Client)query.getSingleResult() ,typeCompteCourant ));
             creerCompte(new CompteBancaire("FR7630004015870002601171228","CompteVoyageItalie",6000,sdf.parse("1960-02-10"),1000,true,(Client)query.getSingleResult() ,typeLivretA ));
             
            query = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", "lolo"); 
             creerCompte(new CompteBancaire("FR7630004015870002601171229","Quotidient",250,sdf.parse("1960-02-10"),500,true,(Client)query.getSingleResult() ,typeCompteCourant ));
             creerCompte(new CompteBancaire("FR7630004015870002601171230","LivretA",5000,sdf.parse("1960-02-10"),1000,true,(Client)query.getSingleResult() ,typeLivretA ));             
             creerCompte(new CompteBancaire("FR7630004015870002601171231","PEL",50000,sdf.parse("1960-02-10"),0,true,(Client)query.getSingleResult() ,typePEL ));
             
             
             

        } catch (ParseException ex) {
            Logger.getLogger(GestionnaireDeCompteBancaire.class.getName()).log(Level.SEVERE, null, ex);
        }
}  

    public void persist(Object object) {
        em.persist(object);
    }
    
    public void update(CompteBancaire c) {
        em.merge(c);
    }
    
    public void updateIsActif(CompteBancaire c){
        c.setActifCB(false);
        update(c);
    }
    
    public void updateIsNotActif(CompteBancaire c){
        c.setActifCB(true   );
        update(c);
    }
    
    public void delete(CompteBancaire c) {
        //delete
    }
    
    
    
}
