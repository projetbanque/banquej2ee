/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Agence;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author l.marchand
 */
@Stateless
@LocalBean
public class GestionnaireDAgence {


    @PersistenceContext(unitName = "BanqueMarchandZhao-ejbPU")
    private EntityManager em;



    public void creerAgence(Agence a) {
        persist(a);
    }

    public List<Agence> getAllAgence() {
        Query query = em.createNamedQuery("Agence.findAll");  
        return query.getResultList();
    }


    
    public void creerAgenceTest() { 
// public Agence(String ville, String codePostal, String rue, int numeroRue, 
//String pays, String email, String telephone, String SwiftCode)
        creerAgence(new Agence("Bidart", "64210","Allée Fauste Elhuyard",0,"France","l.zhao@net.estia.fr","0610839387","AAAFRXXXXX"));  
        creerAgence(new Agence("Paris", "75116","Avenue Montaigne",57,"France","l.marchand@net.estia.fr","0612521905","AAAFRXXXXX")); 
}  

    public void persist(Object object) {
        em.persist(object);
    }
    
    public void update(Agence a) {
        em.merge(a);
    }
}
