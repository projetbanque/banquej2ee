/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Banquier;
import entity.Client;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author l.marchand
 */
@Stateless
@LocalBean
public class GestionnaireDeClient {

    @PersistenceContext(unitName = "BanqueMarchandZhao-ejbPU")
    private EntityManager em;



    public void creerClient(Client c) {
        persist(c);
    }

    public List<Client> getAllClient() {
        Query query = em.createNamedQuery("CompteClient.findAll");  
        return query.getResultList();
    }
    
    /*public Client getClient() {
        Query query = em.createNamedQuery("CompteClient.findByIdentifiant");  
        return (Client) query.getSingleResult();
    }*/
    
    public List<Client> getAllClientOfAgent(String identifiant){
        Query q = em.createQuery("SELECT c FROM Client c WHERE c.banquier.identifiantBanquier=:identifiant");
        q.setParameter("identifiant", identifiant);
        return q.getResultList();
    }


    
    public void creerClientTest() {
        try {
            //String nom, String prenom, String ville, String codePostal, String rue, int numeroRue, String pays, String email, String telephone,
            //String motDePasse, String identifiant, Boolean sexe, Boolean actif, Date dateDeNaissance, int compteDefaultNFC, Banquier banquier
            Query query = em.createNamedQuery("Banquier.findByIdentifiant").setParameter("identifiant", "lzhao");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            
            creerClient(new Client("Jojo","Jennon","Paris","90500","chemin des Pivers",5,"France","jj@gmail.com","0559000011","jojo","jojo","Homme",true,sdf.parse("1960-02-10"),0,(Banquier)query.getSingleResult()));
            
            creerClient(new Client("Toto","Tennon","Clamart","90500","chemin des Tennors",210,"France","tt@gmail.com","0559055022","toto","toto","Homme",true,sdf.parse("1981-08-30"),0,(Banquier)query.getSingleResult()));
            
            creerClient(new Client("Momo","Mennon","Versaille","90500","chemin des Sisi",210,"France","mm@gmail.com","0559055033","momo","momo","Femme",true,sdf.parse("1960-02-10"),0,(Banquier)query.getSingleResult()));
            
            query = em.createNamedQuery("Banquier.findByIdentifiant").setParameter("identifiant", "lmarchand");
            
            creerClient(new Client("Coco","Cennon","Biarritz","64800","chemin des Boucles",210,"France","cc@gmail.com","0559055044","coco","coco","Homme",true,sdf.parse("1960-02-10"),0,(Banquier)query.getSingleResult()));
            
            creerClient(new Client("Vovo","Vennon","Saint Jean de Luz","64500","chemin des Poison",210,"France","vv@gmail.com","0559055055","vovo","vovo","Femme",true,sdf.parse("2000-01-01"),0,(Banquier)query.getSingleResult()));
              
            creerClient(new Client("Lolo","Lennon","Mouguerre","64990","chemin des Arbres",210,"France","ll@gmail.com","0559055066","lolo","lolo","Femme",true,sdf.parse("1996-12-18"),0,(Banquier)query.getSingleResult()));
        } catch (ParseException ex) {
            Logger.getLogger(GestionnaireDeClient.class.getName()).log(Level.SEVERE, null, ex);
        }


        
    } 

    public void persist(Object object) {
        em.persist(object);
    }
    
    public void update(Client c) {
        em.merge(c);
    }

    private void SimpleDateFormat(String yyyyMMjj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Client verifierIdentificationClient(String identifiant, String motDePasse) {
        Query q = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", identifiant);
        Client c = (Client) q.getSingleResult();
        if(c == null) return null;
        if(!c.getMotDePasse().equals(motDePasse)) {
            return null;
        } else {
            return c;
        }
    }
}