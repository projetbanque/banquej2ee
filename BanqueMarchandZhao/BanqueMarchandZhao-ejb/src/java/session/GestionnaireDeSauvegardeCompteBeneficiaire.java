/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Client;
import entity.CompteBancaire;
import entity.SauvegardeCompteBeneficiaire;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author l.marchand
 */
@Stateless
@LocalBean
public class GestionnaireDeSauvegardeCompteBeneficiaire {

 @PersistenceContext(unitName = "BanqueMarchandZhao-ejbPU")
    private EntityManager em;



    public void creerSauvegardeCompteBeneficiaire(SauvegardeCompteBeneficiaire s) {
        persist(s);
    }

    public List<SauvegardeCompteBeneficiaire> getAllSauvegardeCompteBeneficiaire() {
        Query query = em.createNamedQuery("SauvegardeCompteBeneficiaire.findAll");  
        return query.getResultList();
    }
    
    public List<SauvegardeCompteBeneficiaire> getAllSauvegardeCompteBeneficiaireOfClient(Client client) {
        Query query = em.createNamedQuery("SauvegardeCompteBeneficiaire.findByClientProprietaire").setParameter("clientProprietaire", client); 
        return query.getResultList();
    }


    
    public void creerSauvegardeCompteBeneficiaireTest() {
     //public SauvegardeCompteBeneficiaire(CompteBancaire compteBeneficiaire, Client clientProprietaire, String nom) 
        Query query = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171227");
        Query queryClient = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", "lolo");
     
        creerSauvegardeCompteBeneficiaire(new SauvegardeCompteBeneficiaire((CompteBancaire)query.getSingleResult(),(Client)queryClient.getSingleResult(),"Vovo Vennon (Mon Locataire)"));
        
        query = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171223");
        queryClient = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", "lolo");
     
        creerSauvegardeCompteBeneficiaire(new SauvegardeCompteBeneficiaire((CompteBancaire)query.getSingleResult(),(Client)queryClient.getSingleResult(),"Momo Mennon"));
        
        query = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171229");
        queryClient = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", "jojo");
     
        creerSauvegardeCompteBeneficiaire(new SauvegardeCompteBeneficiaire((CompteBancaire)query.getSingleResult(),(Client)queryClient.getSingleResult(),"Lolo Lennon - compteMaison"));
         
        query = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171229");
        queryClient = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", "toto");
     
        creerSauvegardeCompteBeneficiaire(new SauvegardeCompteBeneficiaire((CompteBancaire)query.getSingleResult(),(Client)queryClient.getSingleResult(),"mme L. Lennon (proprio)"));
         
        query = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171229");
        queryClient = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", "momo");
     
        creerSauvegardeCompteBeneficiaire(new SauvegardeCompteBeneficiaire((CompteBancaire)query.getSingleResult(),(Client)queryClient.getSingleResult(),"L.Lennon Bidart"));
         
        query = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171229");
        queryClient = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", "coco");
     
        creerSauvegardeCompteBeneficiaire(new SauvegardeCompteBeneficiaire((CompteBancaire)query.getSingleResult(),(Client)queryClient.getSingleResult(),"LENNON Lolo"));
         
        query = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171229");
        queryClient = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", "vovo");
     
        creerSauvegardeCompteBeneficiaire(new SauvegardeCompteBeneficiaire((CompteBancaire)query.getSingleResult(),(Client)queryClient.getSingleResult(),"lolo lennon location bayonne"));
         
        query = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171227");
        queryClient = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", "momo");
     
        creerSauvegardeCompteBeneficiaire(new SauvegardeCompteBeneficiaire((CompteBancaire)query.getSingleResult(),(Client)queryClient.getSingleResult(),"Vovo Vennon Du Patin"));
         

        
}  

    public void persist(Object object) {
        em.persist(object);
    }
    
    public void update(SauvegardeCompteBeneficiaire s) {
        em.merge(s);
    }
}
