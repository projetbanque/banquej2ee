/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Agence;
import entity.Banquier;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author l.marchand
 */
@Stateless
@LocalBean
public class GestionnaireDeBanquier {
    
    
    @PersistenceContext(unitName = "BanqueMarchandZhao-ejbPU")
    private EntityManager em;



    public void creerBanquier(Banquier b) {
        persist(b);
    }
    
    public Banquier verifierIdentificationBanquier(String identifiant, String motDePasse) {
        Query q = em.createNamedQuery("Banquier.findByIdentifiant").setParameter("identifiant", identifiant);
        Banquier b = (Banquier) q.getSingleResult();
        if(b == null) return null;
        if(!b.getMotDePasseBanquier().equals(motDePasse)) {
            return null;
        } else {
            return b;
        }
    }

    public List<Banquier> getAllBanquier() {
        Query query = em.createNamedQuery("Banquier.findAll");  
        return query.getResultList();
    }
    
    public Banquier getRandomBanquier() {
        //le client est attitré à l'agent LMARCHAND par default...
        int i = (int) Math.random();
        if ((i%2)==0){
            Query query = em.createNamedQuery("Banquier.findByIdentifiant").setParameter("identifiant", "lzhao"); 
            return (Banquier)query.getSingleResult();            
        }else{
            Query query = em.createNamedQuery("Banquier.findByIdentifiant").setParameter("identifiant", "lmarchand"); 
            return (Banquier)query.getSingleResult();
        }
       
    }
    

    
    public void creerBanquierTest() { 
// String nom, String prenom, String email, String telephone, 
//String motDePasse, String identifiant, Boolean sexe, Agence agence
        Query query = em.createNamedQuery("Agence.findById").setParameter("id", 1);
        creerBanquier(new Banquier("Zhao", "Lu","l.zhao@net.estia.fr","0610839387","1111","lzhao","Femme",(Agence)query.getSingleResult()));
        query = em.createNamedQuery("Agence.findById").setParameter("id", 2);
        creerBanquier(new Banquier("Marchand", "Lore","l.marchand@net.estia.fr","0612521905","2222","lmarchand","Femme",(Agence)query.getSingleResult())); 
}  

    public void persist(Object object) {
        em.persist(object);
    }
    
    public void update(Banquier b) {
        em.merge(b);
    }
}
