/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Client;
import entity.CompteBancaire;
import entity.TransactionArgent;
import entity.TypeDeCompte;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author l.marchand
 */
@Stateless
@LocalBean
public class GestionnaireDeTransactionArgent {
 @PersistenceContext(unitName = "BanqueMarchandZhao-ejbPU")
    private EntityManager em;



    public void creerTransactionArgent(TransactionArgent t) {
        persist(t);
    }

    public List<TransactionArgent> getAllTransactionArgent() {
        Query query = em.createNamedQuery("TransactionArgent.findAll");  
        return query.getResultList();
    }
    
    public List<TransactionArgent> getAllTransactionOfClient(String identifiant) {
        //Query queryClient = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", identifiant);
        
        Query q = em.createQuery("SELECT t FROM TransactionArgent t WHERE t.compteDebit.client.identifiant=:identifiant or t.compteCredit.client.identifiant=:identifiant ORDER BY t.dateTransfert DESC");
        q.setParameter("identifiant", identifiant);
        
        return q.getResultList();
    }
    
        
    public List<TransactionArgent> getTransactionOfClientOfCompte(String identifiant,String iBAN) {
        //Query queryClient = em.createNamedQuery("Client.findByIdentifiant").setParameter("identifiant", identifiant);
        
        Query q = em.createQuery("SELECT t FROM TransactionArgent t WHERE (t.compteDebit.client.identifiant=:identifiant or t.compteCredit.client.identifiant=:identifiant) and (t.compteDebit.iBANCB=:iBAN or t.compteCredit.iBANCB=:iBAN)");
        q.setParameter("identifiant", identifiant);
        q.setParameter("iBAN", iBAN);
        
        return q.getResultList();
    }
        
        


    
    public void creerTransactionArgentTest() { 
     try {
         //TransactionArgent(Float sommeTransferee, String libelle, int status, Date dateTransfert, CompteBancaire compteDebit, CompteBancaire compteCredit)
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
         
         Query queryDebit = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171229");
         Query queryCredit = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171231");

         
         creerTransactionArgent(new TransactionArgent(200,"PEL Octobre 2018",2,sdf.parse("2018-10-23"),(CompteBancaire)queryDebit.getSingleResult(),(CompteBancaire)queryCredit.getSingleResult()));
         
               
         queryDebit = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171229");
         queryCredit = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171227");
         
         creerTransactionArgent(new TransactionArgent(650,"loyer Juin 2018",2,sdf.parse("2018-06-01"),(CompteBancaire)queryDebit.getSingleResult(),(CompteBancaire)queryCredit.getSingleResult()));
         
         queryDebit = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171226");
         queryCredit = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171229");
         
         creerTransactionArgent(new TransactionArgent(55,"pour le ticket train",2,sdf.parse("2018-04-02"),(CompteBancaire)queryDebit.getSingleResult(),(CompteBancaire)queryCredit.getSingleResult()));
         
         queryDebit = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171223");
         queryCredit = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171221");
         
         creerTransactionArgent(new TransactionArgent(200,"cadeau anniversaire 18ans",2,sdf.parse("2017-12-12"),(CompteBancaire)queryDebit.getSingleResult(),(CompteBancaire)queryCredit.getSingleResult()));
         
         queryDebit = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171220");
         queryCredit = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171226");
         
         creerTransactionArgent(new TransactionArgent(20,"remboursement resto",2,sdf.parse("2018-02-12"),(CompteBancaire)queryDebit.getSingleResult(),(CompteBancaire)queryCredit.getSingleResult()));
         
         queryDebit = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171227");
         queryCredit = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171228");
         
         creerTransactionArgent(new TransactionArgent(70,"janvier 01 save",2,sdf.parse("2018-01-01"),(CompteBancaire)queryDebit.getSingleResult(),(CompteBancaire)queryCredit.getSingleResult()));
         
         queryDebit = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171221");
         queryCredit = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171222");
         
         creerTransactionArgent(new TransactionArgent(50,"argent pour noel prochain",2,sdf.parse("2018-12-01"),(CompteBancaire)queryDebit.getSingleResult(),(CompteBancaire)queryCredit.getSingleResult()));
         
         queryDebit = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171224");
         queryCredit = em.createNamedQuery("CompteBancaire.findByIBAN").setParameter("iBAN", "FR7630004015870002601171220");
         
         creerTransactionArgent(new TransactionArgent(50,"remboursement autoroute",2,sdf.parse("2018-01-05"),(CompteBancaire)queryDebit.getSingleResult(),(CompteBancaire)queryCredit.getSingleResult()));
         
         
     } catch (ParseException ex) {
         Logger.getLogger(GestionnaireDeTransactionArgent.class.getName()).log(Level.SEVERE, null, ex);
     }
            
        
}  

    public void persist(Object object) {
        em.persist(object);
    }
    
    public void update(TransactionArgent t) {
        em.merge(t);
    }
    
}
