/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import com.sun.istack.Nullable;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author l.marchand
 */
@Entity
@Table(name = "TYPEDECOMPTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeDeCompte.findAll", query = "SELECT t FROM TypeDeCompte t")
    , @NamedQuery(name = "TypeDeCompte.findById", query = "SELECT t FROM TypeDeCompte t WHERE t.id = :id")
    , @NamedQuery(name = "TypeDeCompte.findByNom", query = "SELECT t FROM TypeDeCompte t WHERE t.nomTC = :nom")
})
public class TypeDeCompte implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private Boolean NFCautorise;
    @Nullable
    private float plafond;
    @Column(unique=true)
    private String nomTC;
    @Nullable
    private float transactionMaxDefault;


    //Construteurs
    public TypeDeCompte() {
    }

    public TypeDeCompte(Boolean NFCautorise, float plafond, String nomTC, float transactionMaxDefault) {
        this.NFCautorise = NFCautorise;
        this.plafond = plafond;
        this.nomTC = nomTC;
        this.transactionMaxDefault = transactionMaxDefault;
    }
    
    

    //Methodes
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeDeCompte)) {
            return false;
        }
        TypeDeCompte other = (TypeDeCompte) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.TypeDeCompte[ id=" + id + " ]";
    }
    
    //GET et SET
        public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getNFCautorise() {
        return NFCautorise;
    }

    public void setNFCautorise(Boolean NFCautorise) {
        this.NFCautorise = NFCautorise;
    }

    public float getPlafond() {
        return plafond;
    }

    public void setPlafond(float plafond) {
        this.plafond = plafond;
    }

    public String getNomTC() {
        return nomTC;
    }

    public void setNomTC(String nomTC) {
        this.nomTC = nomTC;
    }

    public float getTransactionMaxDefault() {
        return transactionMaxDefault;
    }

    public void setTransactionMaxDefault(float transactionMaxDefault) {
        this.transactionMaxDefault = transactionMaxDefault;
    }
    
    
    
}
