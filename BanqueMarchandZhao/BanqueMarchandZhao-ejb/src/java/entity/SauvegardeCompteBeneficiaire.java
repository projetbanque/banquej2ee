/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author l.marchand
 */
@Entity
@Table(name = "SAUVEGARDECOMPREBENEFICIAIRE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SauvegardeCompteBeneficiaire.findAll", query = "SELECT s FROM SauvegardeCompteBeneficiaire s")
    , @NamedQuery(name = "SauvegardeCompteBeneficiaire.findById", query = "SELECT s FROM SauvegardeCompteBeneficiaire s WHERE s.id = :id")
    , @NamedQuery(name = "SauvegardeCompteBeneficiaire.findByClientProprietaire", query = "SELECT s FROM SauvegardeCompteBeneficiaire s WHERE s.clientProprietaire = :clientProprietaire")
})
public class SauvegardeCompteBeneficiaire implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne(cascade={CascadeType.MERGE})
    @JoinColumn(name="ID_COMPTEBENEFICIARE",referencedColumnName="ID")
    private CompteBancaire compteBeneficiaire;
    
    public CompteBancaire getCompteBeneficiaire() {
        return compteBeneficiaire;
    }
    
    @ManyToOne(cascade={CascadeType.MERGE})
    @JoinColumn(name="ID_CLIENTPROPRIETAIRE",referencedColumnName="IDClIENT")
    private Client clientProprietaire;
    
    public Client getClientProprietaire() {
        return clientProprietaire;
    }
    
    private String nomSCB;

    public SauvegardeCompteBeneficiaire() {
    }

    public SauvegardeCompteBeneficiaire(CompteBancaire compteBeneficiaire, Client clientProprietaire, String nomSCB) {
        this.compteBeneficiaire = compteBeneficiaire;
        this.clientProprietaire = clientProprietaire;
        this.nomSCB = nomSCB;
    }

 

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SauvegardeCompteBeneficiaire)) {
            return false;
        }
        SauvegardeCompteBeneficiaire other = (SauvegardeCompteBeneficiaire) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.SauvegardeCompteBeneficiaire[ id=" + id + " ]";
    }
    
      public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomSCB() {
        return nomSCB;
    }

    public void setNomSCB(String nomSCB) {
        this.nomSCB= nomSCB;
    }

    
}
