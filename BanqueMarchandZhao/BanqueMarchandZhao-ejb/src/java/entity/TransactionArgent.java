/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author l.marchand
 */
@Entity
@Table(name = "TRANSACTIONARGENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TransactionArgent.findAll", query = "SELECT t FROM TransactionArgent t")
    , @NamedQuery(name = "TransactionArgent.findById", query = "SELECT t FROM TransactionArgent t WHERE t.id = :id")
})

public class TransactionArgent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private float sommeTransferee;
    private String libelle;
    private int status;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateTransfert;
    
    @ManyToOne(cascade={CascadeType.MERGE})
    @JoinColumn(name="ID_COMPTEDEBIT",referencedColumnName="ID")
    private CompteBancaire compteDebit;
    
    public CompteBancaire getCompteDebit() {
        return compteDebit;
    }
    
    @ManyToOne(cascade={CascadeType.MERGE})
    @JoinColumn(name="ID_COMPTECREDIT",referencedColumnName="ID")
    private CompteBancaire compteCredit;
    
    public CompteBancaire getCompteCredit() {
        return compteCredit;
    }
    

    public TransactionArgent() {
    }

    public TransactionArgent(float sommeTransferee, String libelle, int status, Date dateTransfert, CompteBancaire compteDebit, CompteBancaire compteCredit) {
        this.sommeTransferee = sommeTransferee;
        this.libelle = libelle;
        this.status = status;
        this.dateTransfert = dateTransfert;
        this.compteDebit = compteDebit;
        this.compteCredit = compteCredit;
    }



   

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransactionArgent)) {
            return false;
        }
        TransactionArgent other = (TransactionArgent) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.TransactionArgent[ id=" + id + " ]";
    }
    
    
     public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    

    public float getSommeTransferee() {
        return sommeTransferee;
    }

    public void setSommeTransferee(float sommeTransferee) {
        this.sommeTransferee = sommeTransferee;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getDateTransfert() {
        return dateTransfert;
    }

    public void setDateTransfert(Date dateTransfert) {
        this.dateTransfert = dateTransfert;
    }
    
    
    
}
