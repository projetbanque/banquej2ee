/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author l.marchand
 */
@Entity
@Table(name = "AGENCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Agence.findAll", query = "SELECT a FROM Agence a")
    , @NamedQuery(name = "Agence.findById", query = "SELECT a FROM Agence a WHERE a.id = :id")
    , @NamedQuery(name = "Agence.findByVille", query = "SELECT a FROM Agence a WHERE a.villeAgence = :ville")
    , @NamedQuery(name = "Agence.findByEmail", query = "SELECT a FROM Agence a WHERE a.emailAgence = :email")
    , @NamedQuery(name = "Agence.findByTelephone", query = "SELECT a FROM Agence a WHERE a.telephoneAgence = :telephone")
})
public class Agence implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    //nom du propietaire
    private String villeAgence;
    private String codePostalAgence;
    private String rueAgence;
    private int numeroRueAgence;
    private String paysAgence;
    private String emailAgence;
    private String telephoneAgence;
    private String SwiftCodeAgence;
    
    
    
    //Constructeur
        public Agence() {
    }

    public Agence(String villeAgence, String codePostalAgence, String rueAgence, int numeroRueAgence, String paysAgence, String emailAgence, String telephoneAgence, String SwiftCodeAgence) {
        this.villeAgence = villeAgence;
        this.codePostalAgence = codePostalAgence;
        this.rueAgence = rueAgence;
        this.numeroRueAgence = numeroRueAgence;
        this.paysAgence = paysAgence;
        this.emailAgence = emailAgence;
        this.telephoneAgence = telephoneAgence;
        this.SwiftCodeAgence = SwiftCodeAgence;
    }




    
    
    //Méthodes
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Agence)) {
            return false;
        }
        Agence other = (Agence) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Agence[ id=" + id + " ]";
    }
    
    //GET ET SET

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVilleAgence() {
        return villeAgence;
    }

    public void setVilleAgence(String villeAgence) {
        this.villeAgence = villeAgence;
    }

    public String getCodePostalAgence() {
        return codePostalAgence;
    }

    public void setCodePostalAgence(String codePostalAgence) {
        this.codePostalAgence = codePostalAgence;
    }

    public String getRueAgence() {
        return rueAgence;
    }

    public void setRueAgence(String rueAgence) {
        this.rueAgence = rueAgence;
    }

    public int getNumeroRueAgence() {
        return numeroRueAgence;
    }

    public void setNumeroRueAgence(int numeroRueAgence) {
        this.numeroRueAgence = numeroRueAgence;
    }

    public String getPaysAgence() {
        return paysAgence;
    }

    public void setPaysAgence(String paysAgence) {
        this.paysAgence = paysAgence;
    }

    public String getEmailAgence() {
        return emailAgence;
    }

    public void setEmailAgence(String emailAgence) {
        this.emailAgence = emailAgence;
    }

    public String getTelephoneAgence() {
        return telephoneAgence;
    }

    public void setTelephoneAgence(String telephoneAgence) {
        this.telephoneAgence = telephoneAgence;
    }

    public String getSwiftCodeAgence() {
        return SwiftCodeAgence;
    }

    public void setSwiftCodeAgence(String SwiftCodeAgence) {
        this.SwiftCodeAgence = SwiftCodeAgence;
    }


    
}