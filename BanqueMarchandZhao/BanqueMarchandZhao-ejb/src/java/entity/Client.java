/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author l.marchand
 */
@Entity
@Table(name = "CLIENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c")
    , @NamedQuery(name = "Client.findById", query = "SELECT c FROM Client  c WHERE c.idClient = :idClient")
    , @NamedQuery(name = "Client.findByPrenom", query = "SELECT c FROM Client  c WHERE c.prenom = :prenom")
    , @NamedQuery(name = "Client.findByVille", query = "SELECT c FROM Client  c WHERE c.ville = :ville")
    , @NamedQuery(name = "Client.findByEmail", query = "SELECT c FROM Client  c WHERE c.email = :email")
    , @NamedQuery(name = "Client.findByTelephone", query = "SELECT c FROM Client  c WHERE c.telephone = :telephone")
    , @NamedQuery(name = "Client.findByIdentifiant", query = "SELECT c FROM Client  c WHERE c.identifiant = :identifiant")
    , @NamedQuery(name = "Client.findBySexe", query = "SELECT c FROM Client  c WHERE c.sexe = :sexe")
})
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idClient;
    
    //nom du propietaire
    private String nom;
    private String prenom;
    private String ville;
    private String codePostal;
    private String rue;
    private int numeroRue;
    private String pays;
    @Column(unique=true)
    private String email;
    @Column(unique=true)
    private String telephone;
    private String motDePasse;
    @Column(unique=true)
    private String identifiant;
    private String sexe;
    private Boolean actif;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateDeNaissance;
    private int compteDefaultNFC;
    
    
    @ManyToOne(cascade={CascadeType.MERGE})
    @JoinColumn(name="ID_BANQUIER",referencedColumnName="ID")
    private Banquier banquier;
    
    public Banquier getBanquier() {
        return banquier;
    }

    
    
    //Constructeur
        public Client() {
    }

    public Client(String nom, String prenom, String ville, String codePostal, String rue, int numeroRue, String pays, String email, String telephone, String motDePasse, String identifiant, String sexe, Boolean actif, Date dateDeNaissance, int compteDefaultNFC, Banquier banquier) {
        this.nom = nom;
        this.prenom = prenom;
        this.ville = ville;
        this.codePostal = codePostal;
        this.rue = rue;
        this.numeroRue = numeroRue;
        this.pays = pays;
        this.email = email;
        this.telephone = telephone;
        this.motDePasse = motDePasse;
        this.identifiant = identifiant;
        this.sexe = sexe;
        this.actif = actif;
        this.dateDeNaissance = dateDeNaissance;
        this.compteDefaultNFC = compteDefaultNFC;
        this.banquier = banquier;
    }

    
    
    
    //Méthodes
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idClient != null ? idClient.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Client)) {
            return false;
        }
        Client other = (Client) object;
        if ((this.idClient == null && other.idClient != null) || (this.idClient != null && !this.idClient.equals(other.idClient))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Client[ idClient=" + idClient + " ]";
    }
    
    //GET ET SET
  

    public String getNom() {
        return nom;
    }


    public void setNom(String nom) {
        this.nom = nom;
    }


    public Long getIdClient() {
        return idClient;
    }

    public void setIdClient(Long idClient) {
        this.idClient = idClient;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public int getNumeroRue() {
        return numeroRue;
    }

    public void setNumeroRue(int numeroRue) {
        this.numeroRue = numeroRue;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public Boolean getActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Date getDateDeNaissance() {
        return dateDeNaissance;
    }

    public void setDateDeNaissance(Date dateDeNaissance) {
        this.dateDeNaissance = dateDeNaissance;
    }

    public int getCompteDefaultNFC() {
        return compteDefaultNFC;
    }

    public void setCompteDefaultNFC(int compteDefaultNFC) {
        this.compteDefaultNFC = compteDefaultNFC;
    }
    
    
    
    
}