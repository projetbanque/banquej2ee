/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import javax.persistence.Column;

/**
 *
 * @author l.marchand
 */
@Entity
@Table(name = "COMPTEBANCAIRE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CompteBancaire.findAll", query = "SELECT c FROM CompteBancaire c")
    , @NamedQuery(name = "CompteBancaire.findById", query = "SELECT c FROM CompteBancaire c WHERE c.id = :id")
    , @NamedQuery(name = "CompteBancaire.findByNom", query = "SELECT c FROM CompteBancaire c WHERE c.nomCB = :nom")
    , @NamedQuery(name = "CompteBancaire.findBySolde", query = "SELECT c FROM CompteBancaire c WHERE c.soldeCB = :solde")
    , @NamedQuery(name = "CompteBancaire.findByIBAN", query = "SELECT c FROM CompteBancaire c WHERE c.iBANCB = :iBAN")
    , @NamedQuery(name = "CompteBancaire.findByClient", query = "SELECT c FROM CompteBancaire c WHERE c.client = :client")
})
public class CompteBancaire implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(unique=true)
    private String iBANCB;
    private String nomCB;
    //solde du compte
    private float soldeCB;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateCreationCB;
    private float transactionMaxCB;
    private Boolean actifCB;
    
    
    @ManyToOne(cascade={CascadeType.MERGE})
    @JoinColumn(name="ID_CLIENT",referencedColumnName="IDCLIENT")
    private Client client;
    
    public Client getClient() {
        return client;
    }
    
    @ManyToOne(cascade={CascadeType.MERGE})
    @JoinColumn(name="ID_TYPEDECOMPTE",referencedColumnName="ID")
    private TypeDeCompte typeDeCompte;
    
    public TypeDeCompte getTypeDeCompte() {
        return typeDeCompte;
    }
    
    //Constructeur
        public CompteBancaire() {
    }

    public CompteBancaire(String iBANCB, String nomCB, float soldeCB, Date dateCreationCB, float transactionMaxCB, Boolean actifCB, Client client, TypeDeCompte typeDeCompte) {
        this.iBANCB = iBANCB;
        this.nomCB = nomCB;
        this.soldeCB = soldeCB;
        this.dateCreationCB = dateCreationCB;
        this.transactionMaxCB = transactionMaxCB;
        this.actifCB = actifCB;
        this.client = client;
        this.typeDeCompte = typeDeCompte;
    }

   

    
    //Méthodes
    public void deposer(float montant) {  
       soldeCB += montant;  
     }  
    
    public float retirer(float montant) {  
      if (montant < soldeCB) {  
        soldeCB -= montant;  
        return montant;  
      } else {  
        return 0;  
      }  
    } 
    
    public float consulter() {  
       return this.getSoldeCB();  
     }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompteBancaire)) {
            return false;
        }
        CompteBancaire other = (CompteBancaire) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CompteBancaire[ id=" + id + " ]";
    }
    
    //GET ET SET

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getiBANCB() {
        return iBANCB;
    }

    public void setiBANCB(String iBANCB) {
        this.iBANCB = iBANCB;
    }

    public String getNomCB() {
        return nomCB;
    }

    public void setNomCB(String nomCB) {
        this.nomCB = nomCB;
    }

    public float getSoldeCB() {
        return soldeCB;
    }

    public void setSoldeCB(float soldeCB) {
        this.soldeCB = soldeCB;
    }

    public Date getDateCreationCB() {
        return dateCreationCB;
    }

    public void setDateCreationCB(Date dateCreationCB) {
        this.dateCreationCB = dateCreationCB;
    }

    public float getTransactionMaxCB() {
        return transactionMaxCB;
    }

    public void setTransactionMaxCB(float transactionMaxCB) {
        this.transactionMaxCB = transactionMaxCB;
    }

    public Boolean getActifCB() {
        return actifCB;
    }

    public void setActifCB(Boolean actifCB) {
        this.actifCB = actifCB;
    }
   
    
    
    
}