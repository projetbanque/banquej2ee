/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import javax.persistence.ManyToOne;

/**
 *
 * @author l.marchand
 */
@Entity
@Table(name = "BANQUIER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Banquier.findAll", query = "SELECT b FROM Banquier b")
    , @NamedQuery(name = "Banquier.findById", query = "SELECT b FROM Banquier b WHERE b.id = :id")
    , @NamedQuery(name = "Banquier.findByPrenom", query = "SELECT b FROM Banquier b WHERE b.prenomBanquier = :prenom")
    , @NamedQuery(name = "Banquier.findByEmail", query = "SELECT b FROM Banquier b WHERE b.emailBanquier = :email")
    , @NamedQuery(name = "Banquier.findByTelephone", query = "SELECT b FROM Banquier b WHERE b.telephoneBanquier = :telephone")
    , @NamedQuery(name = "Banquier.findByIdentifiant", query = "SELECT b FROM Banquier b WHERE b.identifiantBanquier = :identifiant")
    , @NamedQuery(name = "Banquier.findBySexe", query = "SELECT b FROM Banquier b WHERE b.sexeBanquier = :sexe")
})
public class Banquier implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    //nom du propietaire
    private String nomBanquier;
    private String prenomBanquier;
    @Column(unique=true)
    private String emailBanquier;
    @Column(unique=true)
    private String telephoneBanquier;
    private String motDePasseBanquier;
    @Column(unique=true)
    private String identifiantBanquier;
    private String sexeBanquier;
    
    
    @ManyToOne(cascade={CascadeType.MERGE})
    @JoinColumn(name="ID_AGENCE",referencedColumnName="ID")
    private Agence agence;
    
    public Agence getAgence() {
        return agence;
    }
    
  /*public int getAgence() {
        return id_agence;
    }*/

    
    

    
    
    //Constructeur
        public Banquier() {
    }

    public Banquier(String nomBanquier, String prenomBanquier, String emailBanquier, String telephoneBanquier, String motDePasseBanquier, String identifiantBanquier, String sexeBanquier, Agence agence) {
        this.nomBanquier = nomBanquier;
        this.prenomBanquier = prenomBanquier;
        this.emailBanquier = emailBanquier;
        this.telephoneBanquier = telephoneBanquier;
        this.motDePasseBanquier = motDePasseBanquier;
        this.identifiantBanquier = identifiantBanquier;
        this.sexeBanquier = sexeBanquier;
        this.agence = agence;
    }

   
    
    
    
    //Méthodes
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Banquier)) {
            return false;
        }
        Banquier other = (Banquier) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Banquier[ id=" + id + " ]";
    }
    
    //GET ET SET

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomBanquier() {
        return nomBanquier;
    }

    public void setNomBanquier(String nomBanquier) {
        this.nomBanquier = nomBanquier;
    }

    public String getPrenomBanquier() {
        return prenomBanquier;
    }

    public void setPrenomBanquier(String prenomBanquier) {
        this.prenomBanquier = prenomBanquier;
    }

    public String getEmailBanquier() {
        return emailBanquier;
    }

    public void setEmailBanquier(String emailBanquier) {
        this.emailBanquier = emailBanquier;
    }

    public String getTelephoneBanquier() {
        return telephoneBanquier;
    }

    public void setTelephoneBanquier(String telephoneBanquier) {
        this.telephoneBanquier = telephoneBanquier;
    }

    public String getMotDePasseBanquier() {
        return motDePasseBanquier;
    }

    public void setMotDePasseBanquier(String motDePasseBanquier) {
        this.motDePasseBanquier = motDePasseBanquier;
    }

    public String getIdentifiantBanquier() {
        return identifiantBanquier;
    }

    public void setIdentifiantBanquier(String identifiantBanquier) {
        this.identifiantBanquier = identifiantBanquier;
    }

    public String getSexeBanquier() {
        return sexeBanquier;
    }

    public void setSexeBanquier(String sexeBanquier) {
        this.sexeBanquier = sexeBanquier;
    }
  
    
    
}