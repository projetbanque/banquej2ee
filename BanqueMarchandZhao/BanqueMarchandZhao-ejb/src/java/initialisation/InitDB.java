/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package initialisation;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import session.GestionnaireDeCompteBancaire;
import session.GestionnaireDAgence;
import session.GestionnaireDeBanquier;
import session.GestionnaireDeClient;
import session.GestionnaireDeSauvegardeCompteBeneficiaire;
import session.GestionnaireDeTransactionArgent;
import session.GestionnaireDeTypeDeCompte;


/**
 *
 * @author l.marchand
 */
@Singleton
@Startup // instancie une seule fois des le déploiement
@LocalBean
public class InitDB {

    @EJB
    private GestionnaireDAgence gestionnaireDAgence;
    @EJB
    private GestionnaireDeBanquier gestionnaireDeBanquier;
    @EJB
    private GestionnaireDeClient gestionnaireDeClient;
    @EJB
    private GestionnaireDeTypeDeCompte gestionnaireDeTypeDeCompte;
    @EJB
    private GestionnaireDeCompteBancaire gestionnaireDeCompteBancaire;
    @EJB
    private GestionnaireDeTransactionArgent gestionnaireDeTransactionArgent;
    @EJB
    private GestionnaireDeSauvegardeCompteBeneficiaire gestionnaireDeSauvegardeCompteBeneficiaire;

    
    public InitDB() {
    }
    
    @PostConstruct
    public void initDB() {
        System.out.println("#### initialisation de la DB #####");
        gestionnaireDAgence.creerAgenceTest();
        gestionnaireDeBanquier.creerBanquierTest();
        gestionnaireDeClient.creerClientTest();
        gestionnaireDeTypeDeCompte.creerTypeDeCompteTest();
        gestionnaireDeCompteBancaire.creerComptesTest();
        gestionnaireDeTransactionArgent.creerTransactionArgentTest();
        gestionnaireDeSauvegardeCompteBeneficiaire.creerSauvegardeCompteBeneficiaireTest();
    }

}
